﻿using System;
using System.IO;
using System.Linq;
using Autocomplite;

namespace ConsoleApp
{
    class Program{

        private const string PathToFileParam = "-file";

        static void Main(string[] args)
        {
            if (!args.Contains(PathToFileParam)){
                Console.WriteLine("Неуказан файл для обработки (используейте параметр {0})", PathToFileParam);
                return;
            }
            var filePath = args[Array.IndexOf(args, PathToFileParam) + 1];
            using (var file = new FileStream(filePath, FileMode.Open)){
                new FileWorker(new StreamReader(file)).WorkAllStrings().ToList().ForEach(Console.WriteLine);
            }
        }
    }
}
