﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using Autocomplite;

namespace Server
{
    internal class ClientConection{

        public ClientConection(TcpClient client, FileWorker worker)
        {
            var request = string.Empty;
            var buffer = new byte[1024];
            int count;
            while ((count = client.GetStream().Read(buffer, 0, buffer.Length)) > 0)
            {
                request += Encoding.ASCII.GetString(buffer, 0, count);
                if (request.IndexOf(Environment.NewLine) > -1)
                    break;
            }
            if((request.Split(' '))[0] != "get")
                return;
            var searchResult = new StringBuilder();
            worker.WorkSingleString((request.Split(' '))[1].Replace(Environment.NewLine, "")).ToList()
                        .ForEach(str => searchResult.Append(string.Concat(str, Environment.NewLine)));
            var replyMessage = Encoding.ASCII.GetBytes(searchResult.ToString());
            client.GetStream().Write(replyMessage, 0, replyMessage.Length);
            client.Close();
        }
    }

    class Server{
        private const string PathToFileParam = "-file";

        private const string Port = "-port";

        readonly TcpListener _listener;
        private readonly FileWorker _worker;
        public Server(string pathToFile, int port){
            using (var file = new FileStream(pathToFile, FileMode.Open)){
                _worker = new FileWorker(new StreamReader(file));
            }
                _listener = new TcpListener(IPAddress.Any, port);
                _listener.Start();
                while (true){
                    ThreadPool.QueueUserWorkItem((stateInfo) => {
                        new ClientConection((TcpClient)stateInfo, _worker);
                    }, _listener.AcceptTcpClient());
                }
        }

        ~Server()
        {
            if (_listener != null)
                _listener.Stop();
        }

        static void Main(string[] args)
        {
            if (!args.Contains(PathToFileParam) || !args.Contains(Port)){
                Console.WriteLine(!args.Contains(PathToFileParam) ? 
                    string.Format("Неуказан файл для обработки (используейте параметр {0})", PathToFileParam) :
                    string.Format("Неуказан порт для запуска сервера (используейте параметр {0})", Port));
                return;
            }
            int port;
            int maxThreadsCount = Environment.ProcessorCount * 8;
            ThreadPool.SetMaxThreads(maxThreadsCount, maxThreadsCount);
            ThreadPool.SetMinThreads(2, 2);
            if (int.TryParse(args[Array.IndexOf(args, Port) + 1], out port))
                new Server(args[Array.IndexOf(args, PathToFileParam) + 1], port);
        }
    }
}
