﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Sockets;


namespace Client
{
    class Program
    {
        private const string Port = "-port";
        private const string Host = "-host";

        static void Main(string[] args){
            if (!args.Contains(Port))
                throw new Exception(string.Format("Неуказан порт (используейте параметр {0})", Port));
            if (!args.Contains(Host))
                throw new Exception(string.Format("Неуказан адрес сервера (используейте параметр {0})", Host));
            int port;
            if (!int.TryParse(args[Array.IndexOf(args, Port) + 1], out port))
                throw new Exception("Введеное значение не может быть использовано в качестве порта");
            while (true){
                var userWord = Console.ReadLine();
                Console.WriteLine(GetWords(userWord,
                    args[Array.IndexOf(args, Host) + 1],
                    port));
            }
        }

        static string GetWords(string userWord, string host, int port){
            var receivedBytes = new byte[1024];
            var ipEndPoint = new IPEndPoint(
                Dns.Resolve(host).AddressList[0]
                , port);
            var sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sender.Connect(ipEndPoint);
            sender.Send(Encoding.ASCII.GetBytes(string.Concat("get ", userWord, Environment.NewLine)));
            var totalBytesReceived = sender.Receive(receivedBytes);
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
            return Encoding.ASCII.GetString(receivedBytes, 0, totalBytesReceived);
        }

       
    }
}
