﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Autocomplite
{
    class SheetsRepository : Repository
    {
        private readonly Dictionary<string, int> _sheetsDictionary = new Dictionary<string, int>();

        public override void Insert(string word, int rate)
        {
            _sheetsDictionary.Add(word, rate);
        }

        public override string[] Scan(string prefix, int count = 10)
        {
            return _sheetsDictionary.Where(pair => pair.Key.StartsWith(prefix))
                .OrderByDescending(pair => pair.Value)
                .ThenBy(pair => pair.Key)
                .Take(count)
                .Select(pair => pair.Key)
                .ToArray();
        }
    }
}
