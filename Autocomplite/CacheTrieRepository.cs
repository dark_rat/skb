﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Autocomplite
{
    class CacheTrieRepository: TrieRepository
    {
        private readonly Dictionary<string, string[]> _cache = new Dictionary<string, string[]>();

        public override string[] Scan(string mask, int count = 10)
        {
            if (_cache.ContainsKey(mask))
                return _cache[mask];
            var result = base.Scan(mask, count);
            _cache.Add(mask, result);
            return result;
        }
    }
}
