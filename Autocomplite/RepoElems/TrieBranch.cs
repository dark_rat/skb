﻿using System.Collections.Generic;
using System.Linq;

namespace Autocomplite.RepoElems
{
    /// <summary>
    /// Узел дерева
    /// </summary>
    internal class TrieBranch
    {
        public TrieBranch(char symbol)
        {
            Symbol = symbol;
            Children = new List<TrieBranch>();
        }

        /// <summary>
        /// Ветви дерева
        /// </summary>
        public List<TrieBranch> Children { get; set; }

        /// <summary>
        /// Символ узла
        /// </summary>
        public char Symbol { get; set; }

        /// <summary>
        /// Потомок, соответствующий заданному символу
        /// </summary>
        /// <returns>Узел-потомок либо null, если такого узла нет</returns>
        public TrieBranch this[char symbol]
        {
            get
            {
                return Children.SingleOrDefault(node => node.Symbol == symbol);
            }
        }
    }
}
