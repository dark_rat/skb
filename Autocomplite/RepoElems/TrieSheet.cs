﻿namespace Autocomplite.RepoElems
{
    /// <summary>
    /// Лист дерева
    /// </summary>
    internal class TrieSheet : TrieBranch
    {
        /// <summary>
        /// Конструктор листа (копирует свойства из узла)
        /// </summary>
        public TrieSheet(TrieBranch node, string word, int frequency)
            : base(node.Symbol)
        {
            Children = node.Children;
            Word = word;
            Frequency = frequency;
        }

        /// <summary>
        /// Слово
        /// </summary>
        public string Word { get; set; }

        /// <summary>
        /// Частота появления слова в тексте
        /// </summary>
        public int Frequency { get; set; }
    }
    
}
