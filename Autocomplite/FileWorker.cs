﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Autocomplite
{
    public class FileWorker
    {
        private readonly TextReader _reader;

        private readonly Repository _repositore;

        public FileWorker(TextReader reader)
        {
           
            _reader = reader;
            _repositore = new CacheTrieRepository();
            //чтение слов найденных в текстах
            var wordCount = Convert.ToInt32(_reader.ReadLine());
            while (wordCount-- > 0) {
                _repositore.Insert(_reader.ReadLine());
            }
        }

        public string[] WorkAllStrings(){
            var result = new List<string>();
            //чтение слов, введенных пользователем
            var prefixCount = Convert.ToInt32(_reader.ReadLine());
            while (prefixCount-- > 0)
            {
                //поиск слов, удовлетворяющих запросу
                var words = _repositore.Scan(_reader.ReadLine());
                result.AddRange(words);
                result.Add("");
            }
            return result.ToArray();
        }

        public string[] WorkSingleString(string word)
        {
            //поиск слов, удовлетворяющих запросу
            return _repositore.Scan(word).ToArray();  
        }
    }
}
