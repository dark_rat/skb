﻿using System;

namespace Autocomplite
{
    abstract class Repository
    {
        public void Insert(string word)
        {
            try
            {
                var wordAndRate = word.Split(' ');
                Insert(wordAndRate[0], Convert.ToInt32(wordAndRate[1]));
            }
            catch (Exception e)
            {
                throw new Exception(Utils.Messages.InvalidDictionaryString, e);
            }
        }

        public abstract void Insert(string word, int rate);

        public abstract string[] Scan(string prefix, int count = 10);
    }
}
