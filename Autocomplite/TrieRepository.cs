﻿using System.Collections.Generic;
using System.Linq;
using Autocomplite.RepoElems;

namespace Autocomplite
{
    internal class TrieRepository : Repository
    {
        private readonly TrieBranch _master = new TrieBranch(' ');

        public override void Insert(string word, int frequency)
        {
            var chars = word.ToCharArray();
            var currentNode = _master;
            for (var idx = 0; idx < chars.Length; idx++)
            {
                //получаем ветвь или создаем её
                var symbol = chars[idx];
                var child = currentNode[symbol];
                if (child == null)
                    currentNode.Children.Add(child = new TrieBranch(symbol));

                //последнюю букву в слове заменяем листом
                if (idx == chars.Length - 1)
                {
                    var i = currentNode.Children.IndexOf(child);
                    currentNode.Children[i] = new TrieSheet(child, word, frequency);
                }

                //переходим к дочернему узлу
                currentNode = child;
            }
        }

        public override string[] Scan(string mask, int count = 10)
        {
            return
                Scan(_master, mask)
                    .OrderByDescending(branch => branch.Frequency)
                    .ThenBy(branch => branch.Word)
                    .Take(count)
                    .Select(branch => branch.Word)
                    .ToArray();
        }

        /// <summary>
        /// Рекурсивный обход дерева для поиска слов
        /// </summary>
        /// <param name="branch">Текущий узел дерева</param>
        /// <param name="mask">Префикс слова</param>
        /// <param name="offset">Смещение в префиксе, соответствующее символу текущего узла</param>
        /// <returns>Листья, слова которых начинаются с префикса</returns>
        private IEnumerable<TrieSheet> Scan(TrieBranch branch, string mask, int offset = 0){
            while (true){
                if (offset != mask.Length){
                    //переходим к поддереву, удовлетворяющему следующей части префикса
                    branch = branch[mask[offset]];
                    if (branch == null) yield break;
                    offset = offset + 1;
                    continue;
                }
                //обходим дерево
                foreach (var childBranch in branch.Children.SelectMany(child => Scan(child, mask, mask.Length))){
                    yield return childBranch;
                }
                //возвращаем листья из поддерева
                if (branch is TrieSheet)
                    yield return branch as TrieSheet;
                break;
            }
        }
    }
}
